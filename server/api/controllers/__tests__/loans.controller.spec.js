import test from 'ava';
import mongoose from 'mongoose';

import dummyData from '../../../dummyData';
import { getLoans } from '../loans.controller';

// Create a Test database, and populate it with test data
test.before(async () => {
  // Set the new url
  const MONGO_URL = 'mongodb://localhost:27017/sail-health-meter-test';
  process.env.MONGO_URL = MONGO_URL;

  const promise = new Promise(resolve => {
    mongoose.connect(MONGO_URL, async (error) => {
      if (error) {
        throw error;
      }

      return await resolve(dummyData(10));
    });
  });

  return await promise;
});

// Drop the Test database once the loans test is done
test.after(async () => {
  return mongoose.connection.db.dropDatabase(async () => {
    mongoose.connection.close();
    await Promise.resolve();
  });
});

test('gets an array of loans', async t => {
  const request = { query: {} };
  const response = {
    async send(data) {
      t.true(Array.isArray(data));
    },
  };

  return getLoans(request, response);
});

test('can be limited', async t => {
  const request = { query: { limit: 4 } };

  const response = {
    async send(data) {
      t.true(data.length === 4);
    },
  };

  return getLoans(request, response);
});

test('can be paginated', async t => {
  const request1 = { query: { page: 1, limit: 4 } };
  const request2 = { query: { page: 2, limit: 4 } };

  const firstPage = new Promise(async resolve => {
    const response = { send(data) { resolve(data); } };
    return getLoans(request1, response);
  });

  const secondPage = new Promise(async resolve => {
    const response = { send(data) { resolve(data); } };
    return getLoans(request2, response);
  });

  return Promise.all([firstPage, secondPage])
    .then((loans) => {
      const firstPageResults = loans[0];
      const secondPageResults = loans[1];

      // The first item on each page should be different
      t.not(firstPageResults[0]._id, secondPageResults[0]._id);
    });
});

test('contains a list of transactions for each loan', async t => {
  const request = { query: {} };
  const response = {
    async send(data) {
      if (data && data.length > 0) {
        const loan = data[0];

        t.true(loan.hasOwnProperty('transactions'));
        t.true(Array.isArray(loan.transactions));
        t.true(loan.transactions.length > 0);
      } else {
        t.fail('There were no loans to test!');
      }
    },
  };

  return getLoans(request, response);
});

test('calculates the current health of a loan correctly', async t => {
  const request = { query: {} };
  const response = {
    async send(data) {
      if (data && data.length > 0) {
        const loan = data[0];
        const transactions = data[0].transactions;
        const successfulRepayments = transactions.filter(transaction => transaction.type === 'REPAYMENT');
        const currentHealth = (successfulRepayments.length / transactions.length) * 100;
        t.is(loan.currentHealth, currentHealth);

        // The current health should also match the last transactions' health
        t.is(loan.currentHealth, transactions[transactions.length - 1].health);
      } else {
        t.fail('There were no loans to test!');
      }
    },
  };

  return getLoans(request, response);
});

test('sorts the loans by current health, ascending', async t => {
  const request = { query: {} };
  const response = {
    async send(data) {
      data.forEach((loan, index) => {
        if (index > 0) {
          const previousLoan = data[index - 1];
          t.true(previousLoan.health < loan.health);
        }
      });
    },
  };

  return getLoans(request, response);
});
