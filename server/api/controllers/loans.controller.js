import loan from '../../models/loan';

/**
 * Gets loans, and their related transactions
 *
 * This request is paginated. The paging can be controlled via query parameters (page, and limit)
 *
 * @param req
 * @param res
 * @returns void
 */
export function getLoans(req, res) {
  const aggregate = loan.aggregate();
  aggregate
    // Lookup and join transactions to these loans
    .lookup({
      from: 'transactions',
      localField: '_id',
      foreignField: 'loan_id',
      as: 'transactions',
    })

    // Get the total number of good repayments, and the total number of repayments
    .project({
      _id: true,
      name: true,
      transactions: true,
      currentHealth: {
        $multiply: [
          {
            $divide: [
              {
                $size: {
                  $filter: {
                    input: '$transactions',
                    as: 'transaction',
                    cond: { $eq: ['$$transaction.type', 'REPAYMENT'] },
                  },
                },
              },
              { $size: '$transactions' },
            ],
          },
          100,
        ],
      },
    })

    // After all that, we want to sort it by worst health to best health
    .sort({
      currentHealth: 1,
      'transactions.created_at': 1,
    });

  // Default query params for pagination
  const page = req.query.page || 1;
  const limit = req.query.limit || 10;

  // Paginate the results so that we can get the information when we need it, rather than loading everything
  loan.aggregatePaginate(aggregate, { page, limit }, (err, results) => {
    if (err) {
      // TODO: Add proper error handling/logging
      res.status(500).send('Something went wrong when getting Loans! Oh no!');
    } else {
      const loansResponse = results.map(loanResponse => {
        const mappedLoan = { ... loanResponse };

        // We need to calculate the health of the transactions
        mappedLoan.transactions = mappedLoan.transactions.reduce((accumulator, loanTransaction) => {
          const mappedLoanTransaction = { ... loanTransaction };
          accumulator.push(mappedLoanTransaction);

          // Get the successful repayments, and use it to calculate the health at this date
          const repayments = accumulator.filter(transaction => transaction.type === 'REPAYMENT');
          mappedLoanTransaction.health = (repayments.length / accumulator.length) * 100;

          return accumulator;
        }, []);

        return mappedLoan;
      });

      res.send(loansResponse);
    }
  });
}
