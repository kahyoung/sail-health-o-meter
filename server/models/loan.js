import mongoose from 'mongoose';
import mongooseAggregatePaginate from 'mongoose-aggregate-paginate';

const Schema = mongoose.Schema;

const loanSchema = new Schema({
  name: { type: 'String', required: true },
});

loanSchema.plugin(mongooseAggregatePaginate);

export default mongoose.model('Loan', loanSchema);
