import React, { PropTypes, Component } from 'react';
import styles from './LoanItem.css';

const LineChart = require('react-chartjs').Line;
const chartOptions = {
  scaleBeginAtZero: true,
  scaleOverride: true,
  scaleStartValue: 0,
  scaleSteps: 4,
  scaleStepWidth: 25,
  pointDotStrokeWidth: 1,
  pointHitDetectionRadius: 1,
};

class LoanItem extends Component {
  getColor(val) {
    if (val > 80) return 'green';
    if (val > 50) return 'yellow';
    return 'red';
  }

  getChartInfo(loan) {
    return {
      labels: loan.transactions.map((d, i) => i + 1),
      datasets: [
        {
          label: 'Health',
          fillColor: 'rgba(105,51,147, 0.3)',
          strokeColor: 'rgba(105,51,147, 0.75)',
          pointColor: 'rgba(105,51,147, 1)',
          data: loan.transactions.map(transaction => Math.floor(transaction.health)),
        },
      ],
    };
  }

  render() {
    let loan = this.props.loan;

    return (
      <div key={loan._id} className={styles.loan}>
        <div className={styles.info}>
          <h2>{loan.name}</h2>
          <p>
            Current Health:
            <b style={{ color: this.getColor(loan.currentHealth) }}>
              &nbsp; {Math.floor(loan.currentHealth)}
            </b>
          </p>
        </div>
        <div className={styles.graph}>
          <LineChart data={this.getChartInfo(loan)} options={chartOptions} width="300" height="100" redraw />
        </div>
      </div>
    );
  }
}

LoanItem.propTypes = {
  loan: PropTypes.object.isRequired
};

export default LoanItem;
