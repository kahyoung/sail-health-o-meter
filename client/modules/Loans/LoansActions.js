import callApi from '../../util/apiCaller';

// Export Constants
export const GET_LOANS = 'GET_LOANS';
export const GET_LOANS_PENDING = 'GET_LOANS_PENDING';
export const GET_LOANS_FULFILLED = 'GET_LOANS_FULFILLED';
export const GET_LOANS_REJECTED = 'GET_LOANS_REJECTED';

/**
 * Queries the API and returns a list of loans
 *
 * @param {number} [page=1] - The page that we want to be querying for
 * @return {function(*)}
 */
export function fetchData(page = 1) {
  return (dispatch) => {
    // We need to wrap this in a promise, so that we can return it
    // The current architecture of the application requires a promise to be returned - though this may not be needed anymore
    const promise = new Promise(resolve => {
      callApi(`/loans?page=${page}`)
        .then((response) => {
          resolve(response);
        });
    });

    dispatch({
      type: GET_LOANS,
      payload: promise,
    });

    return promise;
  };
}

