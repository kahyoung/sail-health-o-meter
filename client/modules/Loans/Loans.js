import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';

// Import Actions
import { fetchData } from './LoansActions';
import { DotLoader } from 'react-spinners';
import InfiniteScroll from 'react-infinite-scroller';

// Import Components
import LoanItem from '../../components/LoanItem/index';

import styles from './Loans.css';

// Import Selectors
import { getLoans, isLoading } from './LoansReducer';

// Cache loans so that we're not re-rendering all the time
let loansList = [];

class LoansPage extends Component {
  componentDidMount() {
    this.props.dispatch(fetchData());
  }

  render() {
    const { loans } = this.props;
    if (loans && loans.length) {
      const existingIds = loansList.map(loanItem => loanItem.key);
      const newLoans = loans.filter(loan => !existingIds.includes(loan._id));

      const newLoanItems = newLoans.map((loan) => <LoanItem key={loan._id} loan={loan} />);
      loansList = loansList.concat(newLoanItems);
    }

    let loader = (
      <div className={styles['page-spinner--container']} key={0}>
        <div className={styles['page-spinner']}>
          <DotLoader
            color={'#123abc'}
            loading
          />
        </div>
      </div>
    );

    return (
      <div>
        <p>This is a really fancy list of loans</p>
        <hr />
        <InfiniteScroll
          pageStart={1}
          hasMore
          loadMore={page => fetchData(page)(this.props.dispatch)}
          useWindow
          loader={loader}
        >
          {loansList}
        </InfiniteScroll>

      </div>
    );
  }
}

// Actions required to provide data for this component to render in sever side.
LoansPage.need = [() => { return fetchData(); }];

// Retrieve data from store as props
function mapStateToProps(state) {
  return {
    loans: getLoans(state),
    loading: isLoading(state),
  };
}

LoansPage.propTypes = {
  loans: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

LoansPage.contextTypes = {
  router: React.PropTypes.object,
};

export default connect(mapStateToProps)(LoansPage);
