import { GET_LOANS_PENDING, GET_LOANS_FULFILLED, GET_LOANS_REJECTED } from './LoansActions';

// Initial State
const initialState = { loading: true, data: [] };

const LoansReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LOANS_PENDING :
      return { ... state, loading: true };
    case GET_LOANS_FULFILLED :
      return {
        ... state,
        loading: false,
        data: state.data.concat(action.payload),
      };
    case GET_LOANS_REJECTED :
      return {
        ... state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

/* Selectors */

// Get all loans
export const getLoans = state => state.loans.data;
export const isLoading = state => state.loans.loading;

// Export Reducer
export default LoansReducer;
