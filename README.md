# Sail Health-O-Meter
A dashboard where anyone can quickly view the health of the loan book, and identify problem accounts.

## Environments
Developed on `MacOS 10.13.2` with `Node.js 9.2.1`

#### Compatible/tested Environments
`MacOS 10.13.2, Node.js 9.2.1`

## Dependencies
`Node.js >= 9.2.1` (the only tested version)

## Installation
To get started, `cd` into the root directory and run:

```
npm install
```

## Usage
To run the application for **development**, run the command:

```
npm start
```

To run the application for **production**, run the command:
```
npm run bs
```

## Testing
Testing uses [Ava](https://github.com/avajs/ava). To run tests, run the command:
```
npm run test
```

## Overview
Sail Health-o-meter is a Web Dashboard which shows a list of Loans, ordered from Loans with the least health to Loans with the most health.

Each loan has a number of transactions, and each transaction can have a `type` of `REPAYMENT` or `REPAYMENT_DECLINED`.
`Health` is defined as `(number of successful repayments/total number of repayments) * 100`.

## Discussion
I've developed this app after a total time of ~4.5 hours - over the span of multiple days. I've added the time it took me to reach each milestone in the `git commit` messages.

### Trade offs

#### Development Environment Foundations
In order to develop the application in a production state, I've had to make a few trade offs.

The biggest trade offs I've had to make was to sacrifice the depth of the dashboard's features in order to make sure that the foundations of the application's development environment were working.

For example, the following is a list of things that I had to do before I could get started in adding features:
- `npm test` would not work in `Node 9.2.1` (the current Node version as of this writing)
  - I had to debug and remove the `--harmony-proxies` flag, as it was removed from node
- `npm test` resulted in a number of failed tests
  - This was because there was still references to the original `mern-starter` project
  - Things like the `Footer` component no longer existed, but tests were still looking at it
  - Redundant methods still existed, and were being tested for and failing, as the application did not use it
- `npm run bs` would not successfully start a **production** server, contrary to the `README`
  - This was because `babel-polyfill'` was not being required for production, even though it is [required](https://github.com/babel/babel/issues/5085) as the project has polyfills for ES2015;
  
  
#### Missing Features/Changes
##### Sockets/Subscriptions
One of the keywords of the challenge briefing was _realtime_. Unfortunately, I did not have enough time to implement any kind of real-time access to the DB.

I would have elected to use some derivative of MongoDB's [subscriptions](https://www.mongodb.com/presentations/event-based-subscription-mongodb).

##### Optimise graphs
The list of Loans by themselves are not a big hit on performance - but rendering each graph is.

I wanted to look into alternatives, or options in `react-chartjs` that would minimise this performance hit, as it is a noticeable drop when a graph is initiated.
I was able to compromise on this change, and change the rendering of the graph so that it only renders when the component is mounted - not every time new data came in (as it originally was).

As well as this, I was not happy with the labels of the graph - having singular numbers was not very meaningful to me. 
I would have liked to have put the actual date, but fitting it in the small space was a difficult problem.

##### (Stretch Goal) Search
I wanted to make it even easier to find specific Loan books by implementing a Search functionality - but this would have taken a LOT of time.

##### More tests!
You can never have enough tests. While I did add tests for the loan.controller (which aggregates loans and transactions), I wasn't able to create tests for my LoanItem component which I separated.

### Architectural Decisions

#### Aggregation of data
To minimise the amount of database requests, I elected to use MongoDB's [aggregate](https://docs.mongodb.com/manual/aggregation/) pipelines to 
populate and transform the data in a way that would work best for the dashboard.

This allowed me to link `loans` and `transactions`, as well as calculate and sort by loan's `health`.

#### Pagination
Due to the large amount of data, I believed that the most sensible way of querying the amount of loans was to paginate the data.
Querying thousands of loans in one hit would be way too much traffic in one hit - and most of the data would't be seen on the UI anyway.

#### Infinite Scrolling
As the Loan data is paginated, I did not want to sacrifice the ease of scrolling through data. So, rather than implementing pages, I elected for Infinite scrolling
in order to minimise the extra interactions that users would need to make to view data.

As well as infinite scrolling, I also added loading spinners to make it obvious that data is being fetched.

#### Optimising the rendering of graphs
I noticed that each time new Loan data would come in, every graph would re-draw itself. In the scenario of having 1,000 loans, 
this would be a huge strain on CPU. I decided to cache the list of LoanItem components, so that React would consider
the new loans to be an 'update' and not re-mount the LoanItem component, and hence not re-draw the graph.

As well as this, the original graphs would show multiple data points' data when you hovered over it. I changed this so 
it would only show the data you were hovering over.

#### Conclusions
Overall, I'm happy that I was able to clean up and stabilise the state of the application, and implement a few features. 
I'm also happy that the final product does deliver to the original brief, even if on a very basic level.

I wish I was able to work faster, and implement more features and optimisations, but... :).
